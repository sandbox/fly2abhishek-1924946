/**
 * JavaScript behaviors for the front-end display of safeForms.
 */

(function($) {
  Drupal.behaviors.safeForms = Drupal.behaviors.safeForms || {};

  Drupal.behaviors.safeForms.attach = function(context) {
    // Form temporary storage behaviour behavior.
    Drupal.safeForms.formstore(context);
  };
  Drupal.safeForms = Drupal.safeForms || {};
  Drupal.safeForms.formstore = function(context) {
    var saveToBrowserStorage = function(key, value) {
      try {
        localStorage.setItem(key, value + "");
      } catch (e) {
        //QUOTA_EXCEEDED_ERR
      }
    };
    function init() {
      var href = location.hostname + location.pathname + location.search + location.hash;
      $('form').each(function() {
        var self = this;
        var targetFormId = $(this).attr("id");
        var fieldsToProtect = $(this).find(":input").not(":submit").not(":reset").not(":button").not(":file").not(":hidden").not(":password");
        fieldsToProtect.each(function() {
          // bind formstorge
          var field = $(this);
          var prefix = href + targetFormId + field.attr("name");
          if (field.is(":text") || field.is("textarea")) {
            if (typeof $.browser.msie === 'undefined') {
              field.get(0).oninput = function() {
                saveToBrowserStorage(prefix, field.val());
              };
            } else {
              field.get(0).onpropertychange = function() {
                saveToBrowserStorage(prefix, field.val());
              };
            }
          } else {
            field.change(function() {
              fieldsToProtect.each(function() {
                var field = $(this);
                if (field.attr("name") === undefined) {
                  // Returning non-false is the same as a continue statement in a for loop; it will skip immediately to the next iteration.
                  return true;
                }
                var prefix = href + targetFormId + field.attr("name");
                var value = field.val();
                if (field.is(":checkbox")) {
                  if (field.attr("name").indexOf("[") !== -1) {
                    value = [];
                    $("[name='" + field.attr("name") + "']:checked").each(function() {
                      value.push($(this).val());
                    });
                  } else {
                    value = field.is(":checked");
                  }
                  saveToBrowserStorage(prefix, value);
                } else if (field.is(":radio")) {
                  if (field.is(":checked")) {
                    value = field.val();
                    saveToBrowserStorage(prefix, value);
                  }
                } else {
                  saveToBrowserStorage(prefix, value);
                }
              });
            });
          }
          var resque = localStorage.getItem(prefix);
          if (resque) {
            if (field.attr("name") === undefined) {
              return false;
            }
            if (field.is(":checkbox") && resque !== "false" && field.attr("name").indexOf("[") === -1) {
              field.attr("checked", "checked");
            } else if(field.is(":checkbox") && resque === "false" && field.attr("name").indexOf("[") === -1) {
              field.removeAttr("checked");
            } else if (field.is(":radio")) {
              if (field.val() === resque) {
                field.attr("checked", "checked");
              }
            } else if (field.attr("name").indexOf("[") === -1) {
              field.val(resque);
            } else {
              resque = resque.split(",");
              field.val(resque);
            }
          }
        });
        $(this).bind("submit reset", function() {
          fieldsToProtect.each(function() {
            var field = $(this);
            var prefix = href + targetFormId + field.attr("name");
            localStorage.removeItem(prefix);
          });
        });
      });
    }
    init();
  };
})(jQuery);
