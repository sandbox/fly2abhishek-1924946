Description
-----------
A simple module which stores the data entered into the form so that if the form 
isn’t submitted but the browser is closed the data is still store and then 
retrieved when the user returns to the form.

This module store form data locally within a users browser using HTML5 
localStorage, this means that the data stored in localStorage is still there
even after a user has closed the browser, deleted their cookies or turned off 
their machine/device.

The HTML5 localStorage attribute is supported by most modern browsers including
Firefox 3.5+, Chrome 4+, Safari 4+, Mobile Safari, Android 2+ and 
Internet Explorer 8+.

Requirements
------------
Drupal 7.x

Installation
------------
1. Copy the entire safeform directory the Drupal sites/all/modules directory.

2. Login as an administrator. Enable the module in the "Administer" -> "Modules"

3. Enable the safeform satting at "Administer" -> "Configuration" ->
   "Content authoring" -> "Safeforms settings"


Support
-------
Please use the issue queue for filing bugs with this module at
http://drupal.org/project/issues/safeforms
